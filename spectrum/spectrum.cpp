#include "../math-art.hpp"

string filename = "spectrum.ppm";

unsigned char RD(int i, int j){
	return (i < DIM*2/3.0 && i > DIM/3.0) ?
		0: (i <= DIM/6.0+1 || i >= DIM*5/6.0-1) ?
		255: (i <= DIM/3.0+1) ? 
		255-(int)i*1530/DIM:
		255-(int)(DIM-i)*1530/DIM;
}

unsigned char GR(int i, int j){
	return (i > DIM*2/3.0) ?
		0: (i <= DIM/2.0+1 && i >= DIM/6.0-1) ?
		255: 255-(int)fabs(i-DIM/3.0)*1530/DIM;
}

unsigned char BL(int i, int j){
	return (i < DIM/3.0) ?
		0: (i <= DIM*5/6.0+1 && i >= DIM/2.0-1) ?
		255: 255-(int)fabs(i-DIM*2/3.0)*1530/DIM;
}