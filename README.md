Refer to Tweetable Mathematical Art:
http://codegolf.stackexchange.com/questions/35569/tweetable-mathematical-art

And the modified code here:
http://www.matrix67.com/blog/archives/6039

**1   How to create your source?**

Choose a name for your work, e.g. spectrum

In the project root, make a directory (`mkdir`) with your chosen name.

Change directory (`cd`) to it.

Create your `.cpp` file, and you may refer to `skeleton.cpp` for format.

**2	How to compile your source?**

Change directory (`cd`) to your source path, e.g. spectrum

And do

```
#!shell

cp ../makefile_origin makefile
```

Make sure the name of target file is "makefile".

Inside your source path, run

```
#!shell

make
```
